import setuptools

setuptools.setup(
    name='uldmpy',
    version='1.0',
    packages=['uldmpy'],
    author="Benedikt Franke",
    author_email="benedikt.franke@uni-ulm.de",
    description="A python implementation of the Unconstrained Large Margin Distribution Machine",
    long_description="",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/BenediktFranke/uldmpy",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'numpy',
        'sklearn'
    ],
 )
