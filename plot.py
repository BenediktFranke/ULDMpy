import pandas as pd
from matplotlib import pyplot as plt

df: pd.DataFrame = pd.read_hdf("results/results_merged.h5")
df = df[df["dataset"] != "horsecolic_test"]
df.plot(kind="bar", x="dataset", y=["acc_mean_svm", "acc_mean_uldm"])
plt.savefig("")
plt.show()
