import json
import os
import warnings
from logging import basicConfig, info, INFO

import numpy as np
import pandas as pd
from sklearn.exceptions import DataConversionWarning
from sklearn.model_selection import cross_validate, GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

from uldmpy.LDM import ULDMClassifier


def train_test(X, y, grid, model, dataset_name):
    if model == "uldm":
        pipe = Pipeline([('scale', StandardScaler()), ('cls', ULDMClassifier(verbose=0))])
    else:
        pipe = Pipeline([('scale', StandardScaler()), ('cls', SVC(verbose=0))])

    search = GridSearchCV(pipe, grid, n_jobs=6, verbose=2, cv=3)
    results = cross_validate(search, X, y, cv=5, n_jobs=1, return_estimator=True, verbose=1)

    scores = results['test_score']

    param_sets = [e.best_params_ for e in results['estimator']]
    with open(f"params/{dataset_name}_{model}.json", "a") as fp:
        json.dump(param_sets, fp)

    ret = {'dataset': dataset_name,
           f'acc_mean_{model}': scores.mean(),
           f'acc_std_{model}': scores.std()}

    return ret


warnings.filterwarnings(action='ignore', category=DataConversionWarning)
basicConfig(filename="debug.log", level=INFO, format='%(asctime)s %(message)s')

res_file = "results/results.h5"

if os.path.exists(res_file):
    res = pd.read_hdf(res_file)
else:
    res = pd.DataFrame(columns=['dataset', 'acc_mean_svm', 'acc_std_svm', 'acc_mean_uldm', 'acc_std_uldm'],
                       dtype='float')

store = pd.HDFStore('processed.h5', 'r')

# reversed so wholesale comes last
for ds in store.keys()[::-1]:
        ds = ds[1:] if ds[0] == '/' else ds
        info("started working on {}".format(ds))
        if ds in res['dataset']:
            print("{} is already done!".format(ds))
            info("results existed, skipping!")
            continue
        print("\n\n")
        print("--------------------------------------------------")
        print("working on {}".format(ds))
        res_tmp = {'dataset': ds}
        for model in ["svm", "uldm"]:

            df = pd.read_hdf('processed.h5', key=ds)

            if ds == "horsecolic_train":
                df2 = pd.read_hdf('processed.h5', key="horsecolic_test")
                df = pd.concat((df, df2), ignore_index=True)

            elif ds == "horsecolic_test":
                continue

            X = df.iloc[:, :-1].values
            y = df.iloc[:, -1].values

            if model == 'uldm':
                grid = {
                    'cls__C': np.linspace(1e-6, 1, 100).tolist(),
                    'cls__kernel': ['rbf', 'sigmoid'],
                    'cls__gamma': np.linspace(0.001, 10, 20),
                    'cls__multiclass': ['ovr'],
                    'cls__method': ["numpy"]
                }
            else:
                grid = {
                    'cls__C': np.linspace(0.001, 50, 100).tolist(),
                    'cls__kernel': ['rbf', 'sigmoid'],
                    'cls__gamma': np.linspace(0.001, 10, 20),
                }

            info("started training...")
            results = train_test(X, y, grid, model, ds)
            res_tmp.update(results)

            print("DONE: accuracy={}".format(results[f'acc_mean_{model}']))
            info("DONE: accuracy={}".format(results[f'acc_mean_{model}']))

        res = res.append(res_tmp, ignore_index=True)
        res.to_hdf(res_file, key='results')
