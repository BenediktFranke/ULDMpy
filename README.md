ULDMpy - ULDM in Python

How to use:

clone this repository and run, from a command line in the root of the repository:

```
> pip install dist\uldmpy-1.0-py3-none-any.whl
```

After that, you can import via:

```python
from uldmpy import ULDMClassifier
```

Documentation can be found in uldmpy/LDM.py

IMPORTANT: 

If you want to use the included mahalanobis kernel, you MUST run, from a command line in root:

```
> mahalanobis_setup.bat
```

This must be done separately as it currently only works on Windows.

Repository Structure:

- params: storage for parameter values from the experiments
- results: storage for experiment results
- uldmpy: importable python package with the implementation
- dist: here the whl file for pip-installation is located
- .py files in the root are experiment-scripts