import numpy as np

X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
y = np.array([-1, -1, -1, 1])

from SVM import L1SVMClassifier

svm = L1SVMClassifier(kernel="linear", C=50).fit(X, y)
## #  print(f"{svm.w} * x + {svm.b}")
print(svm.score(X, y))
