import logging
import time
import warnings
from types import FunctionType
from typing import Sequence, Union, List, Tuple, Callable, Dict

import numpy as np
from numpy.linalg import pinv
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.metrics import accuracy_score
from sklearn.metrics.pairwise import linear_kernel, polynomial_kernel, rbf_kernel, sigmoid_kernel


class ULDMClassifier(BaseEstimator, ClassifierMixin):
    """
    A python implementation of the ULDM
    """

    def __init__(self, C: float = 1.0, kernel: Union[str, Callable] = 'rbf', gamma: float = 5.0,
                 degree: int = 2, coef0: float = 0, multiclass: str = 'ovr',
                 method="numpy", verbose: int = 0, logfile=None, **kwargs):
        """
        :param C: SVM-like C parameter

        :param kernel: kernel to be used, one of [linear, poly, gauss, sigmoid], or a function that accepts
                        two arrays as arguments and returns a float

        :param gamma: coefficient for gauss and sigmoid kernel

        :param degree: degree for poynomial kernel

        :param coef0: constant term for sigmoid kernel

        :param multiclass: strategy used to binarize the problem. Currently only ovr is supported.
        Ignored if problem is binary

        :param method: strategy to compute solutions, either "numpy" or "cd" [EXPERIMENTAL!]
                        "numpy": use numpy.linalg.solve, reliable solution quality but may be slow for large datasets
                        "cd": (EXPERIMENTAL) solve by coordinate descent, may yield inferior results

        :param verbose: higher numbers mean more debug output

        :param kwargs: arguments passed to parent mcmpy

        """
        super().__init__(**kwargs)
        self.logfile = logfile
        self.multiclass = multiclass
        self.coef0 = coef0
        self.gamma = gamma
        self.degree = degree
        self.kernel = kernel
        self.C = C
        self.verbose = verbose
        self._K: Callable = linear_kernel
        self.n_samples: int = 0
        self.n_features: int = 0
        self.n_classes: int = 0
        self.X: np.ndarray = np.array([])
        self.ys: np.ndarray = np.array([])
        self._cls: List[Tuple[np.ndarray, float]] = []
        self._fitted: bool = False
        self.classes: Dict = {}
        self._kernel_mat = None
        self._error = lambda x: None
        self._info = lambda x: None
        self._warn = warnings.warn
        self.method = method
        self._c = None
        self._Q = None
        self._m = None

    def fit(self, X: Sequence[Sequence[float]], y: Sequence[int]):
        """
        train classifier on given data

        :param X: feature vectors, 2d np.array

        :param y: target vector, 1d np array. Notice that the MCM is capable of dealing with
        categorical labels of arbitrary type, so passing an array of string labels is permitted,
        as well as numerical classes with missing values

        :return: self: fitted estimator

        """
        if self.verbose > 0:
            if self.logfile is not None:
                logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
            else:
                logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',
                                    filename="svm.log")
            self._error = logging.error
            self._info = logging.info
            self._warn = logging.warning

        if not isinstance(X, np.ndarray):
            X = np.array(X)
        if not isinstance(y, np.ndarray):
            y = np.array(y)

        assert len(X.shape) == 2, "expected len(X.shape) = 2, but got {}. Restructure your data using " \
                                  "np.ndarray.reshape(-1, 1) if your data has a single feature or " \
                                  "np.ndarray.reshape(1, -1) if your data consists of a single point".format(X.shape)
        self.X = X

        assert len(y.shape) == 1

        # param validation
        if self.kernel == "linear":
            self._K = linear_kernel
        elif self.kernel == "poly":
            self._K = lambda x, y=None: polynomial_kernel(x, y, degree=self.degree, coef0=self.coef0)
        elif self.kernel == "rbf":
            self._K = lambda x, y=None: rbf_kernel(x, y, gamma=self.gamma)
        elif self.kernel == "sigmoid":
            self._K = lambda x, y=None: sigmoid_kernel(x, y, gamma=self.gamma, coef0=self.coef0)
        elif self.kernel == "mahalanobis":
            self._K = lambda x, y=None: self.mahalanobis_kernel(X=x, Y=y, gamma=self.gamma)
        elif isinstance(self.kernel, FunctionType):
            self._K = self.kernel
        else:
            self._error("kernel not understood: {}".format(self.kernel))
            raise ValueError("kernel not understood: {}".format(self.kernel))

        assert self.multiclass in ["ovr"], "binarization strategy not understood: {}".format(self.multiclass)
        assert self.method in ["cd", "numpy"]

        # attribute inferring
        self.n_samples = X.shape[0]
        self.n_features = X.shape[1]
        y = self._map_classes(y)
        if self.verbose > 1:
            self._info("detected input classes: {}".format(self.classes))
        self.n_classes = len(self.classes)
        self.ys = self._binarize(X, y)

        start_time = int(round(time.time() * 1000))
        # the actual training
        solve = self._solve_optimization
        self._cls = [solve(self.X, y_p) for y_p in self.ys]
        end_time = int(round(time.time() * 1000))
        if self.verbose > 0:
            self._info("Training took {} milliseconds".format(end_time - start_time))

        # allow prediction and return fitted self as stated by sklearn standard
        self._fitted = True
        return self

    def predict(self, X: Union[Sequence[float], Sequence[Sequence[float]]], y=None) -> np.ndarray:
        """
        predict labels for given feature vectors

        :param X: feature vector(s), 1d or 2d np.array

        :param y: ignored (here for compabillity reasons)

        :return: np.array of shape (len(X),) with predicted labels (same dtype as the labels had when passed to fit)

        """
        if not self._fitted:
            raise RuntimeError("The classifier was not fitted prior to calling predict(). "
                               "Call fit on the training data before attempting to predict.")

        if not isinstance(X, np.ndarray):
            X = np.array(X)

        assert len(X.shape) == 2, "expected len(X.shape) = 2, but got {}. Restructure your data using " \
                                  "np.ndarray.reshape(-1, 1) if your data has a single feature or " \
                                  "np.ndarray.reshape(1, -1) if your data consists of a single point".format(X.shape)

        if self.n_classes == 2:
            result = self._vote_binary(X)
        elif self.multiclass == "ovr":
            result = self._vote_ovr(X)
        # elif self.multiclass == "ovo":
        #    result = self._vote_ovo(X)
        else:
            raise ValueError("binarization strategy not understood: {}".format(self.multiclass))
        # map class -1 to class 0
        if -1 in result:
            assert 0 not in result, "class -1 and class 0 were found in results, is this a regression task?\nResults: " \
                                    "{}".format(result)
            result[result == -1] = 0

        return np.array([self.classes[x] for x in result.astype(np.int16)])

    def score(self, X: Sequence[Sequence[float]], y: Sequence[float], **kwargs) -> float:
        """
        evaluate accuracy of estimator on given test data

        :param X: test feature vectors

        :param y: test targets

        :param kwargs: ignored (here for compabillity reasons)

        :return: a single scalar, the mean accuracy on the given data

        """
        return accuracy_score(y, self.predict(X))

    # noinspection PyArgumentList
    def _solve_optimization(self, X: np.ndarray, y: np.ndarray) -> Tuple[np.ndarray, float]:
        """
        train classifier on given data

        :param X: feature vectors, 2d np.array

        :param y: target vector, 1d np array

        :return: tuple of (alphas, b, support, h) with alphas being a 1d np.array of length n_samples, b being a float,

        X being the support vectors as np.ndarray

        """

        # prepare matrices
        self._kernel_mat = self._K(X)
        # h
        K_y = np.mean(self._kernel_mat * y[:, np.newaxis], axis=0, keepdims=True)
        y_mean = np.array([[y.mean()]])
        h = np.squeeze(np.hstack((K_y, y_mean)))

        # K (which is not the kernel matrix)
        # This would consume 325 GB of memory for 3400 samples, so we only use it if n_samples < 1000
        if self.n_samples < 1000:
            K_2 = np.mean(self._kernel_mat[:, :, np.newaxis] * self._kernel_mat[:, np.newaxis, :], axis=0)
        else:
            K_2 = np.ones((self.n_samples, self.n_samples))
            for i in range(self.n_samples):
                K_2 *= np.outer(self._kernel_mat[i], self._kernel_mat[i])
        K_slash = np.mean(self._kernel_mat, axis=0, keepdims=True)

        K_p1 = np.vstack((K_2 - np.outer(K_y, K_y), K_slash - y_mean * K_y))
        K_p2 = np.vstack((K_slash.T - y_mean * K_y.T, 1 - y_mean ** 2))
        K = np.hstack((K_p1, K_p2))

        CIK = self.C * np.identity(self.n_samples + 1) + K

        assert np.all(CIK == CIK.T)

        betas = np.linalg.solve(CIK, h)

        alphas = betas[:-1]
        b = betas[-1]

        return alphas, b

    def _binarize(self, X: np.ndarray, y: np.ndarray) -> np.ndarray:
        """
        creates binary classification problems from input data

        :param X: feature vectors

        :param y: target vector

        :return: array of ys, equal-indexed as the binarized labels

        """
        if self.n_classes == 2:
            y = y.copy()
            y[y == 0] = -1
            return np.array([y])

        elif self.multiclass == "ovr":
            ys = np.array([(y == c).astype(int) for c in range(self.n_classes)])
            ys[ys == 0] = -1
            return ys

        else:
            raise ValueError("binarization strategy not understood: {}".format(self.multiclass))

    def _vote_binary(self, X: np.ndarray) -> np.ndarray:
        """
        evaluate the single classifier trained for a binary problem

        :param X: feature vectors

        :return: np.array of length len(X) with predicted class labels

        """
        assert len(self._cls) == 1
        alphas, b = self._cls[0]
        kernel_mat = self._K(X, self.X)
        cls = np.array([
            np.sign(np.sum([a_j * kernel_mat[i, j] for j, a_j in enumerate(alphas)]) + b)
            for i, _ in enumerate(X)
        ])
        if 0 in cls:
            cls[cls == 0] = 1
            if all(cls == 0) and self.verbose > 0:
                warnings.warn("WARNING: All predicted samples lie on the decision line. Something went terribly wrong!")
        return cls

    def _vote_ovr(self, X: np.ndarray) -> np.ndarray:
        """
        evaluate the n ovr classifiers

        :param X: feature vectors

        :return: np.ndarray of len(X) with predicted class labels

        """
        K = self._K(X, self.X)
        scores = np.array([
            K @ alphas[:, np.newaxis] + b for alphas, b in self._cls
        ])
        result = np.argmax(np.squeeze(scores), axis=0)
        assert len(result) == len(X)
        return result

    def _map_classes(self, y: np.ndarray, train=True) -> np.ndarray:
        """
        maps class labels to int. If train == True, also sets the class mapping in the field self.classes

        :param y: target vector

        :param train: if True, set self.classes, else just do the remapping

        :return: mapped target vector of dtype int16

        """
        if train:
            self.classes = np.unique(y)
        y_remaped = np.fromiter(map(lambda x: np.where(self.classes == x)[0].item(), y), dtype=np.int16)
        return y_remaped

    def mahalanobis_kernel(self, X: np.ndarray, Y: Union[None, np.ndarray] = None, gamma: float = 1.0):
        try:
            from uldmpy.kernel.kernel import mahalanobis_kernel_c
        except:
            raise Exception("The mahalanobis kernel needs a separate setup (\"mahalanobis_setup.bat\" and is "
                            "currently only supporting Windows!")
        if Y is None or np.array_equal(X, Y):
            # training time, set attributes
            self._c = np.mean(X, axis=0)
            self._Q = np.linalg.pinv(np.cov(X, rowvar=False, bias=True))
            d = X - self._c[np.newaxis, :]
            self._m = np.mean([np.inner(np.inner(x -self._c, self._Q),
                                                      x - self._c) for x in X])

        if Y is None:
            return mahalanobis_kernel_c(X, X, self._Q, self._m, gamma)
        else:
            return mahalanobis_kernel_c(X, Y, self._Q, self._m, gamma)

