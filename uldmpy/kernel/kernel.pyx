#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

import numpy as np
cimport numpy as np
from libc.stdlib cimport malloc
from libc.math cimport exp

cdef void prod1(int n_features, double[:] x, double[:] y, double[:, :] Q, double[:] result):
    for i in range(n_features):
        result[i] = 0
        for j in range(n_features):
            result[i] += (x[j] - y[j]) * Q[i, j]


cdef double prod2(int n_features, double[:] h, double[:] x, double[:] y):
    cdef double result = 0;
    for i in range(n_features):
        result += h[i] * (x[i] - y[i])

    return result


cdef void _mahalanobis_kernel(int n_features, int n_samples1, int n_samples2, double[:, :] X, double[:, :] Y, double[:, :] Q, double m,
                              double gamma, double[:, :] result):
    cdef double[:] tmp = <double[:n_features]> malloc(sizeof(double) * n_features)
    for i in range(n_samples1):
        for j in range(n_samples2):
            prod1(n_features, X[i], Y[j], Q, tmp)
            result[i, j] = exp(-1 * (gamma/m) * prod2(n_features, tmp, X[i], Y[j]))

    # free(<void*>tmp)


def mahalanobis_kernel_c(np.ndarray[double, ndim=2, mode="c"] X, np.ndarray[double, ndim=2, mode="c"] Y,
                         np.ndarray[double, ndim=2, mode="c"] Q, double m, double gamma):
    cdef int n_features = X.shape[1]
    cdef int n_samples1 = X.shape[0]
    cdef int n_samples2 = Y.shape[0]
    cdef np.ndarray[double, ndim=2, mode="c"] result = np.zeros((n_samples1, n_samples2))
    _mahalanobis_kernel(n_features, n_samples1, n_samples2, memoryview(X), memoryview(Y), memoryview(Q), m, gamma, memoryview(result))
    return np.asarray(result)
