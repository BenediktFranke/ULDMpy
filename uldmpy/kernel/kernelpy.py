import numpy as np


def mahalanobis_kernel(X, Y, Q_inv, m, gamma):
    result = np.zeros((X.shape[0], Y.shape[0]))
    for i in range(X.shape[0]):
        for j in range(Y.shape[0]):
            d = X[i] - Y[j]
            result[i, j] = np.exp((-gamma / m) * d @ Q_inv @ d)

    return result


def prod_1(d, Q):
    result = np.zeros((d.shape[0],))
    for i in range(d.shape[0]):
        result[i] = 0
        for j in range(d.shape[0]):
            result[i] += d[j] * Q[j][i]
    return result


def prod_2(h, d):
    result = 0
    for i in range(h.shape[0]):
        result += h[i] * d[i]
    return result


def mahalanobis_naive(X, Y, Q_inv, m, gamma):
    result = np.zeros((X.shape[0], Y.shape[0]))
    d = np.zeros((X.shape[1]))
    for i in range(X.shape[0]):
        for j in range(Y.shape[0]):
            for k in range(X.shape[1]):
                d[k] = X[i, k] - Y[j, k]

            result[i][j] = np.exp((- gamma / m) * prod_2(prod_1(d, Q_inv), d))

    return result
