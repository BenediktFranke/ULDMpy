import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport abs
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)

def coordinate_descent_uldm(np.ndarray kernel_mat, np.ndarray y, double C, int maxiter, double tol):
    # h
    K_y = np.mean(kernel_mat * y[:, np.newaxis], axis=0, keepdims=True)
    y_mean = np.array([[y.mean()]])
    h = np.squeeze(np.hstack((K_y, y_mean)))

    # K (which is not the kernel matrix)
    K_2 = np.mean(kernel_mat[:, :, np.newaxis] * kernel_mat[:, np.newaxis, :], axis=0)
    K_slash = np.mean(kernel_mat, axis=0, keepdims=True)
    K_p1 = np.vstack((K_2 - np.outer(K_y, K_y), K_slash - y_mean * K_y))
    K_p2 = np.vstack((K_slash.T - y_mean*K_y.T, np.power(1 - y_mean, 2)))
    K = np.hstack((K_p1, K_p2))

    cdef double[:] hc = h
    cdef double[:, :] CIK = C * np.identity(len(kernel_mat) + 1) + K

    cdef double[:, :] beta_cur = np.ones((1, len(K)))
    cdef double cur_val = 0.5 * inner(matmul(beta_cur, CIK)[0], beta_cur[0]) - inner(hc, beta_cur[0])

    cdef int d = kernel_mat.shape[0]
    cdef double new_beta_i

    for _ in range(maxiter):
        for i in range(d):
            # calculated by taking derivate of ULDM function and solving for f'(b) = 0
            new_beta_i = (-1 * inner(beta_cur[0], CIK[:, i]) + beta_cur[0, i] * CIK[i, i]) / (2 * CIK[i, i])
            beta_cur[0, i] = new_beta_i

        prev_val = cur_val
        cur_val = 0.5 * inner(matmul(beta_cur, CIK)[0], beta_cur[0]) - inner(hc, beta_cur[0])

        if abs(cur_val - prev_val) < tol:
            break

    return np.asarray(beta_cur[0], dtype=np.float64)


cdef double[:, :] matmul(double[:, :] A, double[:, :] B):
    cdef size_t n, m, p
    n = A.shape[0]
    m = A.shape[1]
    p = B.shape[2]

    B = B.T
    cdef double[:, :] result = np.zeros((n, p), dtype=np.float64)
    for i in range(n):
        for j in range(p):
            for k in range(p):
                result[i, j] += A[i, k] + B[j, k]

    return result

cdef double inner(double[:] a, double[:] b) nogil:
    cdef size_t n = a.shape[1]
    cdef double result = 0.0
    for i in range(n):
        result += a[i] * b[i]

    return result




