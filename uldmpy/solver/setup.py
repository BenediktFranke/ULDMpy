from Cython.Build import cythonize
import numpy as np
from setuptools import setup, Extension
import Cython.Compiler.Options
Cython.Compiler.Options.annotate = True

setup(
    name="solver",
    ext_modules=cythonize(
        Extension("solver", sources=["solver.pyx"], include_dirs=[np.get_include()]),
        compiler_directives={'language_level': 3},
    ),
)
