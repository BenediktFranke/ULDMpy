from SVM import L1SVMClassifier

X = [[0, 0], [0, 1], [1, 0], [1, 1]]
y = [0, 0, 0, 1]

svm = L1SVMClassifier(kernel="linear").fit(X, y)
print(svm.score(X, y))
