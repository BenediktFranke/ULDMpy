from unittest import TestCase

from sklearn.datasets import load_iris, load_breast_cancer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
import numpy as np

from uldmpy.LDM import ULDMClassifier
from uldmpy.kernel.kernel import mahalanobis_kernel
from uldmpy.kernel.kernelpy import mahalanobis_naive


class TestLDM(TestCase):
    def test1(self):
        X = [[0, 0], [0, 1], [1, 0], [1, 1]]
        y = [0, 0, 0, 1]
        ldm = ULDMClassifier(kernel="linear", verbose=2, C=0.1)
        ldm.fit(X, y)
        score = ldm.score(X, y)
        self.assertEqual(1.0, score)

    def test_iris(self):
        X, y = load_iris(True)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
        ldm = ULDMClassifier(kernel="rbf", verbose=2, C=0.1).fit(X_train, y_train)
        score = ldm.score(X_test, y_test)
        print(score)
        self.assertGreater(score, 0.95)

    def test_breast_cancer(self):
        X, y = load_breast_cancer(True)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
        scaler = StandardScaler()
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.fit_transform(X_test)
        ldm = ULDMClassifier(kernel="rbf", verbose=2, C=0.1).fit(X_train, y_train)
        score = ldm.score(X_test, y_test)
        print(score)
        self.assertGreater(score, 0.95)

        svm = SVC().fit(X_train, y_train)
        svm_score = svm.score(X_test, y_test)
        print(svm_score)
        self.assertGreaterEqual(score, svm_score)

    def test_mahalanobis_kernel(self):
        X = np.array([[10, 1], [0, 2], [1, 1], [1, 2]], dtype=np.float64)
        c = np.mean(X, axis=0)
        Q = np.linalg.pinv(np.cov(X, rowvar=False, bias=True))
        m = np.mean([np.inner(np.inner(x - c, Q), x - c) for x in X])
        res = mahalanobis_kernel(X, X, Q, m, 1.0)

        # carefully crafted by hand
        expected = np.array([[1., 0.03997455, 0.01923123, 0.06200825],
                             [0.03997455, 1., 0.06200825, 0.95239017],
                             [0.01923123, 0.06200825, 1., 0.03997455],
                             [0.06200825, 0.95239017, 0.03997455, 1.]])

        np.testing.assert_array_almost_equal(expected, res)

    def test_mahalanobis_kernel_time(self):
        X, _ = load_breast_cancer(True)
        c = np.mean(X, axis=0)
        Q = np.linalg.pinv(np.cov(X, rowvar=False, bias=True))
        m = np.mean([np.inner(np.inner(x - c, Q), x - c) for x in X])
        res = mahalanobis_kernel(X, X, Q, m, 1.0)
        # runs in 1/3 of runtime of the the numpy-based version

