import numpy as np

X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
y = np.array([-1, 1, 1, 1])
C = 10

from gurobipy import Model, GRB, quicksum

kernel_mat = X.dot(X.T)

# create model
model = Model("SVM")

# add variables
alphas = model.addVars(len(X), lb=0, ub=C)

# add objective
obj = quicksum(alphas) - 0.5 * quicksum(quicksum(alphas[i] * alphas[j] * y[i] * y[j] * kernel_mat[i, j] for j in range(len(X))) for i in range(len(X)))

model.setObjective(obj, sense=GRB.MAXIMIZE)
model.update()

# add constraint
lhs = quicksum(alphas[i] * y[i] for i in range(len(X)))
sense = GRB.EQUAL
rhs = 0
model.addConstr(lhs=lhs, sense=sense, rhs=rhs, name="constraint")

model.update()
model.optimize()

status = model.getAttr('Status')
print("Optimization terminated with code {}".format(status))

# save optimized alphas and bias
alphas_out = np.array([a.x for a in np.squeeze(alphas.values())])
w = np.sum(a_i * y_i * x_i for a_i, y_i, x_i in zip(alphas_out, y, X))
b = -0.5 * (np.max(X[y == -1].dot(w)) + np.min(X[y == 1].dot(w)))

m = -w[0]/w[1]
t = -b/w[1]

print(f"calculated line is y={m}x + {t}")
