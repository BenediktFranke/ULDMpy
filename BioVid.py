import numpy as np
from genetic_selection import GeneticSelectionCV
from scipy.io import loadmat
from sklearn.decomposition import PCA
from sklearn.model_selection import GridSearchCV, LeaveOneGroupOut, cross_validate
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from uldmpy.LDM import ULDMClassifier

# load the data
data = loadmat("BioVid.mat")
ids = data["ID"]
X = data["data"]
y = np.squeeze(data["labels"])

# prepare a results file
res_file = "pain_results.log"

# only keep classes 0 and 4
inds = np.where((y == 0) | (y == 4))[0]
ids = np.squeeze(ids[inds])
X = X[inds]
y = y[inds]

# -----------------------------------------------------------------------------------
# regular CV section

# prepare pipeline
pipe = Pipeline([("pca", PCA(whiten=True)), ("scale", StandardScaler()), ("cls", ULDMClassifier())])

# prepare param grid
grid = {
    'cls__C': np.geomspace(1e-6, 2, 20),
    'cls__kernel': ['rbf'],
    'cls__gamma': np.geomspace(0.001, 2, 20),
    'cls__verbose': [0],
    'pca__n_components': [20, 25]
}

# random search
search = GridSearchCV(pipe, grid, n_jobs=5, cv=2, verbose=2)
results = cross_validate(search, X, y, cv=LeaveOneGroupOut(), groups=ids, n_jobs=1, return_estimator=True)

# results
print("RESULTS:")
print(results['test_score'].mean())

with open(res_file, 'a+') as fp:
    fp.write("------------EXPERIMENT------------\n")
    fp.write("GridSearch")
    fp.write("Pipeline:\n")
    fp.write(str(pipe.named_steps))
    fp.write("\nparams:\n")
    fp.write(str(list(map(lambda x: x.best_params_, results['estimator']))))
    fp.write("\naccuracy: {}\n".format(results['test_score'].mean()))

