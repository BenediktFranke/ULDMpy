import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib import rc

# not_plot = ["horsecolic_train"]

rc('text', usetex=False)

df1 = pd.read_hdf("results/results_maha.h5")[["dataset", "acc_mean_uldm"]]
df2 = pd.read_hdf("results/results_merged.h5")[["dataset", "acc_mean_uldm"]]
df1.columns = ["dataset", "maha"]
df2.columns = ["dataset", "regular"]
df1["dataset"] = df1["dataset"].astype(str)
df2["dataset"] = df2["dataset"].astype(str)
df = df1.merge(df2, how="inner", on="dataset")

df.loc[df['dataset'] == 'Wholesale customers data', 'dataset'] = "wholesale"
df.loc[df['dataset'] == 'horsecolic_train', 'dataset'] = "horsecolic"

df1 = pd.melt(df[['dataset', 'acc_mean_svm', 'acc_mean_uldm']], id_vars=['dataset'], value_name="acc_mean")\
    .sort_values(['dataset'])

df1['variable'] = ["ULDM" if v == "acc_mean_uldm" else "SVM" for v in df1["variable"]]
sns.set_style("whitegrid")
# g = sns.barplot(x='dataset', y='acc_mean', hue='variable', data=df1, orient="v")
g = sns.barplot(y='dataset', x='acc_mean', hue='variable', data=df1, orient="h")
plt.xlim((0.4, 1.0))
plt.xlabel("accuracy")
#plt.xticks(rotation=90)
g.legend(loc='center left', bbox_to_anchor=(1.01, 0.5), ncol=1)
plt.tight_layout()

#plt.show()
plt.savefig("results.png", dpi=2000)
